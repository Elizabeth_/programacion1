import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import json
import os
import os.path as path

def archivojson():
    try:
        with open('compuestos2.json', 'r') as file:
            data = json.load(file)
        file.close()
    except IOError:
        data = []
    return data


def save_file(data):
    print("Save File")

    with open('compuestos2.json', 'w') as file:
        json.dump(data, file, indent=2)
    file.close()
    
class VentanaLista():
	def __init__(self):
		print("dadasd")
		self.builder = Gtk.Builder()
		self.builder.add_from_file("compuestos2.glade")

		self.ventana1= self.builder.get_object("ventanaLista")
		self.ventana1.connect("destroy", Gtk.main_quit)
		self.ventana1.set_default_size(600, 400)
		self.ventana1.set_title("Ventana Principal")

		self.boton_editar = self.builder.get_object("editar")
		self.boton_editar.connect("clicked", self.Editar)
		
		self.boton_eliminar = self.builder.get_object("eliminar")
		self.boton_eliminar.connect("clicked", self.Eliminar)
		
		self.boton_agregar = self.builder.get_object("agregar")
		self.boton_agregar.connect("clicked", self.Agregar)
		
		self.boton_cerrar = self.builder.get_object("cerrar")
		self.boton_cerrar.connect("clicked", self.Cerrar)
		
		self.expanderOr = self.builder.get_object("organico")
		self.expanderIn = self.builder.get_object("inorganico")
		
		self.listaOrganicos = self.builder.get_object("mostrarOr")
		self.listaInorganicos = self.builder.get_object("mostrarIn")
		
		self.listmodel = Gtk.ListStore(str, str)
		self.mostrarOrganicos = self.builder.get_object("mostrarO") 
		self.mostrarOrganicos.set_model(model=self.listmodel)
		
		self.listmodel2 = Gtk.ListStore(str, str)
		self.mostrarInorganicos = self.builder.get_object("mostrarI") 
		self.mostrarInorganicos.set_model(model=self.listmodel2)
		cell = Gtk.CellRendererText ()
		cell2 = Gtk.CellRendererText ()
		title = ("Nombre", "estructura")
		
		for i in range(len(title)):
			col = Gtk.TreeViewColumn(title[i], cell, text=i)
			self.mostrarOrganicos.append_column(col)
			col2 = Gtk.TreeViewColumn(title[i], cell2, text=i)
			self.mostrarInorganicos.append_column(col2)
		
		self.remove_all_data()
		self.remove_all_data2()
		self.show_all_data()
		self.show_all_data2()
		self.ventana1.show_all()
		
	def show_all_data(self):
		data = archivojson()
		for i in data[0]['Organico']:
			x = [i[0], i[1]] 
			self.listmodel.append(x)
			
	def show_all_data2(self):
		data = archivojson()
		for i in data[0]['Inorganico']:
			x = [i[0], i[1]] 
			self.listmodel2.append(x)
			
	def remove_all_data(self):
		if len (self.listmodel) != 0:
			for i in range(len(self.listmodel)):
				iter = self.listmodel.get_iter(0)
				self.listmodel.remove(iter)
		
	def remove_all_data2(self):
		if len (self.listmodel2) != 0:
			for i in range(len(self.listmodel2)):
				iter = self.listmodel2.get_iter(0)
				self.listmodel2.remove(iter)
			
	def Agregar(self, btn=None):
		d = VentanaAgregar()
		respuesta = d.ventana.run()
		if respuesta == 1:
			self.remove_all_data()
			self.remove_all_data2()
			self.show_all_data()
			self.show_all_data2()
				
	def Eliminar(self, btn=None):
		VentanaLista.destroy()
		print("hola")
	def Editar(self, btn=None):
		print("hola2")
		
	def Cerrar(self, btn=None):
		self.ventana1.destroy()
		
class VentanaAgregar ():
	def __init__(self):
		
		self.builder = Gtk.Builder()
		self.builder.add_from_file("compuestos2.glade")
		self.ventana= self.builder.get_object("ventana2")
		self.ventana.set_default_size(600, 400)
		self.ventana.set_title("Ventana Principal")
		self.ventana.show_all()
		self.entryNombre = self.builder.get_object("nombre")
		self.entryEstructura = self.builder.get_object("estructura")
		self.comboselec = self.builder.get_object("comboclasificacion")
		  
		self.boton_cancelar = self.builder.get_object("botonCancelar")
		self.boton_cancelar.connect("clicked", self.cancelar)

		self.boton_aceptar = self.builder.get_object("botonAceptar")
		self.boton_aceptar.connect("clicked", self.aceptar)
		
	def cancelar(self , btn= None):
		self.ventana.destroy()
		
	def aceptar(self , btn= None):
		nombre = self.entryNombre.get_text()
		estructura = self.entryEstructura.get_text()
		clasificacion = self.comboselec.get_active_text()
		data = archivojson()
		if not data:
			dic = {"Inorganico": [],
					"Organico": []}
			data.append(dic)
		if clasificacion == 'Inorganico':
			data[0]['Inorganico'].append([nombre, estructura])
		
		elif clasificacion == 'Organico':
			data[0]['Organico'].append([nombre, estructura])
			
		save_file(data)
		self.ventana.destroy()
		
class clasificar():
	def __init__(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("compuestos.glade")
		
		self.boton_aceptar = self.builder.get_object("aceptar")
		self.boton_aceptar.connect("clicked", self.aceptar)
		
		self.ventana= self.builder.get_object("clasificacion")
		self.ventana.set_default_size(600, 400)
		self.ventana.set_title("Clasificacion")

		self.treeclasificado = self.builder.get_object("clasificar")

		self.listmodel = Gtk.ListStore(str, str, str)
		self.treeclasificado.set_model(model=self.listmodel)
      
		cell = Gtk.CellRendererText()
		title = ("Nombre", "Estructura", "organicos","inorganicos")
        
		for i in range(len(title)):
			col = Gtk.TreeViewColumn(title[i], cell, text=i)
			self.treeclasificado.append_column(col)

		self.show_all_data()
		self.ventana.show_all()
        
	def show_all_data(self):
		data2= archivojsonclasificar()
		for i in data2:
			x = [x for x in i.values()]
			self.listmodel.append(x)
	def aceptar():
		self.clasificacion.destroy()
		
# Funcion main en python
if __name__ == "__main__":
	d = VentanaLista()
	Gtk.main()
  

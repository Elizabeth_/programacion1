
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import matplotlib.pyplot as plt

import json
##arreglar pacientes de cada doctor.
#que no se pueda ingrasar una fecha ya pasada.

def archivojson():
    try:
        with open('Pacientes.json', 'r') as file:
            data = json.load(file)
        file.close()
    except IOError:
        data = []
    return data


def save_file(data):
    print("Save File")

    with open('Pacientes.json', 'w') as file:
        json.dump(data, file, indent=5)
    file.close()
# Archivo json para guardar doctores
def archivojsonDoctores():
    try:
        with open('doctores.json', 'r') as file:
            data2 = json.load(file)
        file.close()
    except IOError:
        data2 = []
    return data2


def save_fileDoctores(data2):
    print("Save File")

    with open('doctores.json', 'w') as file:
        json.dump(data2, file, indent=5)
    file.close()

def  llenar_combo ( combo , data2 ):

    lista = Gtk.ListStore ( str )

    for i in data2:
        lista.append ([i])

    combo.set_model (lista)
    renderer_text = Gtk.CellRendererText ()
    combo.pack_start (renderer_text, True )
    combo.add_attribute (renderer_text, "text" , 0 )

def obtener_valor_combobox(combo):
    tree_iter = combo.get_active_iter()
    if tree_iter is not None:
        model = combo.get_model()
        seleccion = model[tree_iter][0]

    return seleccion
    
    
class IngresarDoctores ():
    def __init__(self):	
        self.builder = Gtk.Builder()
        self.builder.add_from_file("Registro3.glade")

        self.doctores = self.builder.get_object("UsuarioD")
        self.doctores.set_default_size(600, 400)
        self.botoon_cancel = self.builder.get_object("Cancelar")
        self.botoon_cancel.connect("clicked", self.button_cancel_clicked)
        
        self.botoon_aceptar = self.builder.get_object("Aceptar")
        self.botoon_aceptar.connect("clicked", self.button_ok_clicked)
        
        self.doctorIngresado = self.builder.get_object("Doctor")
        self.doctores.show_all()
        
    def button_cancel_clicked(self , btn=None):
        self.doctores.destroy()
        
    def button_ok_clicked(self , btn=None):
        data2 = archivojsonDoctores()
        if len(data2) < 3: 
            doctores = self.doctorIngresado.get_text()
            data2.append(doctores)
            save_fileDoctores(data2)
            self.doctores.destroy()
            print (len(data2))
            if len(data2) != 3 :   
                e = IngresarDoctores()
                        
            else:
                self.doctores.destroy() 
                e = VentanaPrincipal()


class VentanaPrincipal():
	
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("Registro3.glade")
        self.ventana0 = self.builder.get_object("VentanaPrincipal")
        self.ventana0.connect("destroy", Gtk.main_quit)
        self.ventana0.set_default_size(600, 400)
        self.ventana0.set_title("INICIO DEL PROGRAMA")
        
        #Botones
        self.boton_secretaria = self.builder.get_object("secretaria")
        self.boton_secretaria.connect("clicked", self.Secretaria)
        self.boton_doctor = self.builder.get_object("doctor")
        self.boton_doctor.connect("clicked", self.Doctor)
        
        
        self.ventana0.show_all()
        
    def Secretaria(self, event):
        Usuario = 'secretaria'
        d = VentanaInicio(Usuario)
		
    def Doctor(self, event):
        p = usuariosD()

     
class  usuariosD (): 
	
    def __init__(self):
       self.builder = Gtk.Builder()
       self.builder.add_from_file("Registro3.glade")

       self.usuariosD = self.builder.get_object("UsuarioDoctores")
       self.usuariosD.set_default_size(600, 400)
       self.usuariosD.set_title(" Usuarios ")
       
       self.boton_aceptar = self.builder.get_object("botonAceptar")
       self.boton_aceptar.connect("clicked", self.botonAceptar)
        
       self.Doctores = self.builder.get_object("listaDoctores")
       data2 = archivojsonDoctores()
       llenar_combo(self.Doctores, data2)
       self.usuariosD.show_all()
        
    def botonAceptar (self, btn= None):        
        D_selec = obtener_valor_combobox(self.Doctores)
        p = VentanaDoctor(D_selec)


class VentanaDoctor():
    def __init__(self, D_selec):
        self.D_selec = D_selec
        self.builder = Gtk.Builder()
        self.builder.add_from_file("Registro3.glade")
        
        self.ventanadoctor = self.builder.get_object("ventanadoctor")
        self.ventanadoctor.set_default_size(600, 400)
        
        self.ventanadoctor.set_title("Pacientes del " +str(self.D_selec))
        self.Verpaciente = self.builder.get_object("verpaciente")
        self.Verpaciente.connect("clicked", self.VentanaDiagnostico)
        self.treepacientes = self.builder.get_object("pacientes")
        self.listmodel = Gtk.ListStore(str, str, str, str, str, str, str, str, str, str)
        self.treepacientes.set_model(model=self.listmodel)
        cell = Gtk.CellRendererText()
        title = ("Hora", "Nombre", "Rut","Apellido","Doctor", "anio", "mes", "dia", "estado", "diagnostico")
       
        for i in range(len(title)):
            col = Gtk.TreeViewColumn(title[i], cell, text=i)
            self.treepacientes.append_column(col)

        self.show_all_data()
        self.ventanadoctor.show_all()
        
    def show_all_data(self):
		
        data = archivojson()
        for i in range (len(data)): 
            if data [i]['doctor'] == self.D_selec: 
                x = [x for x in data[i].values()]
                print(x)
                self.listmodel.append(x)
            
    def VentanaDiagnostico(self, btn=None):

        model, it = self.treepacientes.get_selection().get_selected()
        if model is None or it is None:
            return
        data = archivojson()
        
        for paciente in range(len(data)):
			# con python 3.7 cambiar hora por rut
            if data[paciente]['hora'] == model.get_value(it, 0):
                self.ventanadoctor.destroy()
                p = Diagnostico(self.D_selec, paciente)
        
        
class Diagnostico ():
	
    def __init__(self, D_selec, paciente):
		
        self.paciente = paciente
        self.D_selec = D_selec
        self.builder = Gtk.Builder()
        self.builder.add_from_file("Registro3.glade")

        self.ventanadiag = self.builder.get_object("VentanaDiagnostico")
        self.ventanadiag.set_title("Pacientes del" +str(self.D_selec))
        self.labelnombre = self.builder.get_object("entrynombre")
        self.labelapellido = self.builder.get_object("entryapellido")
        self.labelrut = self.builder.get_object("entryrut")
        self.labelhora = self.builder.get_object("entryhora")
        self.labeldia = self.builder.get_object("entrydia")
        self.labelmes = self.builder.get_object("entrymes")
        self.labelano = self.builder.get_object("entryano")
        self.labeldoctores = self.builder.get_object("entrydoctor")
        self.entrydiagnostico = self.builder.get_object("entrydiagnostico")
        self.aceptar = self.builder.get_object("botonaceptar")
        self.aceptar.connect("clicked", self.guardar_diagnostico)

        data = archivojson()
        self.labelnombre.set_text(data[self.paciente]['nombre'])
        self.labelapellido.set_text(data[self.paciente]['apellido'])
        self.labelrut.set_text(data[self.paciente]['rut'])
        self.labelhora.set_text(data[self.paciente]['hora'])
        self.labeldia.set_text(data[self.paciente]['dia'])
        self.labelmes.set_text(data[self.paciente]['mes'])
        self.labelano.set_text(data[self.paciente]['anio'])
        self.labeldoctores.set_text(self.D_selec)
        self.ventanadiag.show_all()
        
    def guardar_diagnostico(self, btn=None):
		
        if(self.entrydiagnostico != ""):
            diagonostico = self.entrydiagnostico.get_text()
            data = archivojson()
            data[self.paciente]['estado']= 'Atendido'
            data[self.paciente]['diagnostico']= diagonostico
            save_file(data)
            self.ventanadiag.destroy()
            p = VentanaDoctor(self.D_selec)
  
                
class VentanaInicio():
	
    def __init__(self, Usuario):
        
        self.usuario = Usuario
        
        self.builder = Gtk.Builder()
        self.builder.add_from_file("Registro3.glade")

        self.window = self.builder.get_object("VentanaInicio")
        self.window.connect("destroy", Gtk.main_quit)
        self.window.set_default_size(600, 400)
        self.window.set_title("Gestion de horas para Pacientes")

        self.button_open_dialog = self.builder.get_object("IngresarPaciente")
        self.button_open_dialog.connect("clicked", self.agregar)
        
        self.button_buscar = self.builder.get_object("BuscarPaciente")
        self.button_buscar.connect("clicked", self.buscarpaciente)
        
        self.rutpaciente = self.builder.get_object("RutPaciente")
        
        self.button_listap = self.builder.get_object("ListadePacientes")
        self.button_listap.connect("clicked", self.listap)
        

        self.button_search = self.builder.get_object("BuscarPD")
        self.window.show_all()

    def buscarpaciente (self, btn=None):
		
        rut = self.rutpaciente.get_text()
        d = VentanaBuscador(rut)
		
    def agregar(self, btn=None):
		
        print("Aprete el boton add")
        d = DialogoPaciente()
        response = d.dialogo.run()

        if response == Gtk.ResponseType.OK:
            print("Aprete OK")
            
        elif response == Gtk.ResponseType.CANCEL:
            print("Aprete Cancelar")


    def listap(self, btn=None):
		
      d = MostrarListaPacientes()
               
       
    def buscar(self, bnt=None):
		
         buscar = BusquedaDoctor()
   
        
class VentanaBuscador():
	
    def __init__(self, rut):
		
        self.rut = rut
        self.builder = Gtk.Builder()
        self.builder.add_from_file("Registro3.glade")
        
        self.buscador = self.builder.get_object("Buscador")
        
        self.boton_eliminar = self.builder.get_object("eliminar")
        self.boton_eliminar.connect("clicked", self.eliminar)
        
        self.boton_editar = self.builder.get_object("editar")
        self.boton_editar.connect("clicked", self.editar)
        
        self.listmodel = Gtk.ListStore(str, str, str, str, str, str, str, str, str, str)
        self.treeResultado = self.builder.get_object("treeResultado1")
        self.treeResultado.set_model(model=self.listmodel)

        cell = Gtk.CellRendererText()
        title = ("Hora", "Nombre", "Rut","Apellido","Doctor","dia", "mes", "anio", "estado", "diagnostico")
        for i in range(len(title)):
            col = Gtk.TreeViewColumn(title[i], cell, text=i)
            self.treeResultado.append_column(col)

        self.mostrar()    
        self.buscador.show_all()
        
    
    def eliminar(self, bnt=None):
		# se eliminan los pacientes , pero hay que cerrar la ventana y volver a abrirla para ver que se elimino
        data = archivojson()
        for i in range(len(data)):
            if(self.rut == data[i]['rut']):
                it = i
                data.pop(it)
                save_file(data)
        self.mostrar()
        
		
    def editar(self, btn=None):
	
		# se abre la ventana DialogoPaciente y se pueden editar los datos, sin embargo se bebe cerrar el archivo y ver 
		# el cambio, y no se borran los prieros datos ingresados. 
        d = DialogoPaciente()

        model, it = self.treeResultado.get_selection().get_selected()
        if model is None or it is None:
            return
        d.rut.set_text(model.get_value(it, 2))
        d.nombre.set_text(model.get_value(it, 1))
        d.apellido.set_text(model.get_value(it, 3))
        d.doctor.set_text(model.get_value(it, 4))
        d.hora.set_text(model.get_value(it, 0))

        response = d.dialogo.run()

        if response == Gtk.ResponseType.OK:
            print("Aprete OK")
            self.remove_all_data()
            self.eliminar()
            self.show_all_data()
        elif response == Gtk.ResponseType.CANCEL:
            print("Aprete Cancelar")
        
		
    def mostrar(self):
		
        data = archivojson()
        for i in range(len(data)):
            if(self.rut == data[i]['rut']):
                busqueda = data[i].values()
                self.listmodel.append(busqueda)
        self.buscador.show_all()
   
          
class MostrarListaPacientes ():
	
	# se nos va a permitir tener un conocimiento de todos los pacientes registrados
    def __init__(self):
               
        self.builder = Gtk.Builder()
        self.builder.add_from_file("Registro3.glade")
        self.VentanaPacientes = self.builder.get_object("ListaPacientes")
        self.VentanaPacientes.set_title("Lista de Pacientes")
               
        self.listmodel = Gtk.ListStore(str, str, str, str, str, str, str, str, str, str)
        self.treeResultado = self.builder.get_object("treeResultado")
        self.treeResultado.set_model(model=self.listmodel)
        self.boton_grafico = self.builder.get_object("grafico")
        self.boton_grafico.connect("clicked", self.graficar)
        cell = Gtk.CellRendererText()
        title = ("Hora", "Nombre", "Rut","Apellido","Doctor", "anio", "mes", "dia", "estado", "diagnostico")
        for i in range(len(title)):
            col = Gtk.TreeViewColumn(title[i], cell, text=i)
            self.treeResultado.append_column(col)

        self.show_all_data()
        self.VentanaPacientes.show_all()
        
    def show_all_data(self):
    
        data = archivojson()
        for i in data:
            x = [x for x in i.values()]
            self.listmodel.append(x)
            
      
    def graficar(self, btn=None): 
    
        data2 = archivojsonDoctores()
        doctor = []
        num_pacientes = []
        for i in range(len(data2)):
            doctor.append(data2[i])
        data = archivojson()
		
        for k in range(len(doctor)):
            contador = 0
            doctor_selecc = doctor[k]
            for i in range(len(data)):
                if data[i]['doctor'] == doctor_selecc:
                    contador = contador + 1
            num_pacientes.append(contador)
		        					
        plt.title('Grafico de Pacientes por Doctor')
        plt.bar(doctor, num_pacientes, color=(0,1,1), align='center')
        plt.ylabel("Numero de pacientes")
        plt.xlabel("Medicos")
        plt.show()
   
    	                       
class DialogoPaciente():

    def __init__(self):
      
        # Creamos un objeto builder para manipular Gtk
        self.builder = Gtk.Builder()
        # Agregamos los objetos Gtk disenados en Glade
        self.builder.add_from_file("Registro3.glade")

        self.dialogo = self.builder.get_object("dialogoPaciente")
        self.dialogo.show_all()

        self.button_cancel = self.builder.get_object("buttonCance")
        self.button_cancel.connect("clicked", self.button_cancel_clicked)
        
        self.calendario = self.builder.get_object("calendario")
        
        self.button_ok = self.builder.get_object("buttonOk")
        self.button_ok.connect("clicked", self.button_ok_clicked)

        
        self.hora = self.builder.get_object("entryHora")
        self.nombre = self.builder.get_object("entryNombre")
        self.rut = self.builder.get_object("entryRut")
        self.rut.connect("changed", self.verificarRut)

        self.apellido = self.builder.get_object("entryApellido")
        self.doctor = self.builder.get_object("doctores")
        data2 = archivojsonDoctores()
        llenar_combo(self.doctor, data2)
       
    def verificarRut(self, btn=None):
        rut = self.rut.get_text()
        data = archivojson()
        for i in range (len(data)):
            if data[i]["rut"] == rut:
                self.rut.set_text("")
                self.dialogo.destroy()
                p = VerificarRut()		
		
    def button_ok_clicked(self, btn=None):
        anio, mes, dia = self.calendario.get_date()
        hora = self.hora.get_text()
        nombre = self.nombre.get_text()
        rut = self.rut.get_text()
        apellido = self.apellido.get_text()
        doctor = obtener_valor_combobox(self.doctor)
        
        j = {"hora": hora,
            "nombre": nombre,
            "rut": rut, 
            "apellido": apellido, 
            "doctor": doctor,
            "anio": str(anio),
            "mes":str(mes + 1) ,
            "dia":str(dia), 
            "estado": "No atendido",
			"diagnostico": " "
            }
            
        f = archivojson()
        f.append(j)
        save_file(f)
     
        self.dialogo.destroy()

    def button_cancel_clicked(self, btn=None):
        
        self.dialogo.destroy()
  
class VerificarRut():
    
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("Registro3.glade")
        self.rutrut = self.builder.get_object("Rutrepetido")
        self.botonaceptar2 = self.builder.get_object("BotonAceptar")
        self.botonaceptar2.connect("clicked", self.cerrarVentana)
        
        self.rutrut.show_all()

    def cerrarVentana(self, btn=None): 
    
        self.rutrut.destroy()
        p = DialogoPaciente()
     
        
class BusquedaDoctor():

    def __init__(self):
        # Creamos un objeto builder para manipular Gtk
        self.builder = Gtk.Builder()
        # Agregamos los objetos Gtk disenados en Glade
        self.builder.add_from_file("Registro3.glade")

        self.dialogo = self.builder.get_object("BusquedaD")
        self.dialogo.show_all()

        self.button_cancel = self.builder.get_object("entryCancelar")
        self.button_cancel.connect("clicked", self.button_cancel_clicked)
        
        self.button_buscar = self.builder.get_object("BuscarDoctor")
        self.button_buscar.connect("clicked", self.button_busqueda)
        
        self.button_ok = self.builder.get_object("entryAceptar")
        self.button_ok.connect("clicked", self.button_ok_clicked)

        self.busqueda = self.builder.get_object("busqueda")
        self.label_datos = self.builder.get_object("datos")
        
    def button_busqueda(self, btn=None):
		
        busqueda = self.busqueda.get_text()
        f = archivojson()
        for i in range(len(f)):
            if busqueda == f[i]["doctor"]:
                paciente = "Paciente: " + f[i]["nombre"]
                texto_label = self.label_datos.get_text() 
                texto_label += '\n' + paciente
                self.label_datos.set_text(texto_label)
	  
    def button_ok_clicked(self, btn=None):
		
        self.dialogo.destroy()

    def button_cancel_clicked(self, btn=None):
		
        self.dialogo.destroy()

		
if __name__ == "__main__":
	
    data2 = archivojsonDoctores()
    
    if len(data2) != 3: 
        p = IngresarDoctores()
    else: 
        p = VentanaPrincipal()
    Gtk.main()

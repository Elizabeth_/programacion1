﻿BIENVENIDOS AL MANUAL DEL LABERINTO "ESCAPA SI PUEDES" 

Este laberinto ha sido creado para el entretenimiento, pueden jugarlo personas de todas las edades y solo necesitas tener instalado en tu computador python versión nueva desde el 3 hacia adelante para poder ejecutarlo sin problema.                      

Al ejecutar el programa nos llevará directamente al menú en donde habrá que elegir la opción que queramos jugar en números, seleccionamos uno de ellos (con enter) y el programa nos llevará a la pantalla seleccionada. 

Este juego comienza con un símbolo que será nuestro personaje para poder huir del laberinto, esto es de forma individual y si llegas a tu destino, en hora buena! , ganaste. 

REGLAS
Estas son las reglas que debes tener en cuenta al momento de jugar:
Movimientos hacia arriba --> pulsas "8" 
Movimientos hacia abajo --> pulsas "2"
Movimientos hacia la derecha--> pulsas "6"
Movimientos hacia la izquierda--> pulsas "4"

¡Precauciones! 
No presiones otra tecla que no esté en la lista de movimientos, o si no, no avanzarás.

Ambiente del codigo para creación del laberinto (reglas PEP8, para el programador usadas en el proyecto). 

Estas son algunas de las reglas más básicas de PEP8 (Python Enhancement Proposals 8, Propuestas para mejorar Python)que se emplearon y estas más que reglas, son acuerdos y consejos a seguir para lograr un código ordenado y legible, que permita ser entendido por cualquier programador.

Indentar con 4 espacios por nivel.

Nunca mezclar espacios con tabuladores.(Se sugiere indentar con espacios)

Limitar los 79 caracteres por linea. 

Separar funciones de alto nivel y definiciones de clase con dos líneas en blanco.

Los métodos definidos dentro de una clase están separados por una línea en blanco.

Use líneas en blanco en funciones, con moderación, para indicar secciones lógicas.

Las importaciones por lo general deben estar en líneas separadas.

Evite dejar espacios en blanco en las siguientes situaciones.

-Dentro de paréntesis curvos, llaves, o paréntesis rectos.

-Antes de una coma o punto y coma.

-Antes del paréntesis que abre la lista de argumentos en el llamado a una función.

-Más de un espacio alrededor de una asignación del operador para alinearlo con otro.
Use espacios alrededor de los operadores aritméticos.
No utilice espacios alrededor del signo '=' cuando se usa para indicar un argumento o un valor de parámetros.
Los comentarios que contradicen el código son peores que si no hubiera comentarios. Tenga como prioridad mantener los comentarios actualizados con los cambios del código. # doble espacio y el argumento






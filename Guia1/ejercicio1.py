import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class ventanaUno():
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("ventana.glade")

        self.window = self.builder.get_object("window")
        self.window.set_default_size(800, 600)
        self.window.connect("destroy", Gtk.main_quit)

        self.primerTexto = self.builder.get_object("primerTexto")
        self.segundoTexto = self.builder.get_object("segundoTexto")
        self.button_accept = self.builder.get_object("accept")
        self.button_Reiniciar = self.builder.get_object("reset")
 
        for evento in ["activate", "changed"]:
            self.primerTexto.connect(evento, self.actualizar)
            self.segundoTexto.connect(evento, self.actualizar)

        self.button_accept.connect("clicked", self.infoSuma)
        self.button_Reiniciar.connect("clicked", self.confirmar_reinicio)

        self.window.show_all()

    def actualizar(self,btn=None):
        primerTexto = self.primerTexto.get_text()
        segundoTexto = self.segundoTexto.get_text()
        VALORSUMA = len(primerTexto) + len(segundoTexto)

    def infoSuma(self, btn=None):
        primerTexto = self.primerTexto.get_text()
        segundoTexto = self.segundoTexto.get_text()
        VALORSUMA = len(primerTexto) + len(segundoTexto)
        x =ventanaDos(primerTexto, segundoTexto, VALORSUMA)
        x.window2.run()
        x.primetTexto.set_text("sumatextos")
        x.segundoTexto.set_text("sumatextos")
		
    def confirmar_reinicio(self, btn=None):
        dialog = Gtk.MessageDialog(type=Gtk.MessageType.WARNING, buttons=Gtk.ButtonsType.YES_NO)
        dialog.format_secondary_text("Se eliminaran los datos anteriores")

        def respuesta(dialogo, respuesta):
            dialog.close()

            if respuesta == Gtk.ResponseType.YES:
                self.Reiniciar()

        dialog.connect("response", respuesta)
        dialog.run()

    def Reiniciar(self,btn=None):
        self.primerTexto.set_text("")
        self.segundoTexto.set_text("")
        self.actualizar()
        
class ventanaDos():
	
    def __init__(self, primerTexto, segundoTexto, VALORSUMA):
        self.primerTexto = primerTexto
        self.segundoTexto = segundoTexto
        self.X = VALORSUMA
        
        length = len(primerTexto) + len(segundoTexto)
        text = "Texto 1:\n" + primerTexto
        text += "\n\nTexto 2:\n" + segundoTexto
        text += "\n\nLargo de ambas cadenas: " + str(length)

        self.builder = Gtk.Builder()
        self.builder.add_from_file("ventana.glade")

        self.window2 = self.builder.get_object("VentanaAceptar")
        self.window2.set_default_size(400, 300)
        self.window2.connect("destroy", Gtk.main_quit)
      
        self.respuesta = self.builder.get_object("sumatextos")
        self.respuesta2 = self.builder.get_object("valorSuma")     
        self.button_aceptar2 = self.builder.get_object("aceptar2")
        self.button_aceptar2.connect("clicked", self.boton_aceptar2) 
       
        suma = self.primerTexto + self.segundoTexto 
        self.respuesta.set_text(suma)      
        self.respuesta2.set_value(VALORSUMA)
        self.window2.show_all()
        
    def boton_aceptar2(self, evento):
        self.window2.destroy()

if __name__ == "__main__":
    w = ventanaUno()
    Gtk.main()

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import json
import os , sys
directorioPymol = "/usr/bin/pymol/modules"
# Se setea el directorio de pymol
sys.path.insert(0,directorioPymol)
os.environ['PYMOL_PATH'] = os.path.join(directorioPymol, 'pymol/pymol_path')
import pymol
from pymol import cmd


def archivojson():
    try:
        with open('listamoleculas.json', 'r') as file:
            data = json.load(file)
        file.close()
    except IOError:
        data = []
    return data


def save_file(data):
	print("Save File")

	with open('listamoleculas.json', 'w') as file:
		json.dump(data, file, indent=2)
		file.close()


class Menu():
	def __init__(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("moleculas.glade")
		self.ventana0 = self.builder.get_object("menu")
		self.ventana0.connect("destroy", Gtk.main_quit)
		self.ventana0.set_default_size(600, 400)
		self.ventana0.set_title("Ventana Principal")

		self.boton_directorio= self.builder.get_object("directorio1")
		self.boton_directorio.connect("clicked", self.BotonDirectorio)
		

		self.boton_visualizarM = self.builder.get_object("vizualizarmoleculas1")
		self.boton_visualizarM.connect("clicked", self.VerMoleculas)
		
		self.textview_descripcion = self.builder.get_object("descripcion")
		self.imagenmolecula = self.builder.get_object("imagen1")
		
		self.boton_cancelar = self.builder.get_object("botonCancelar")
		self.boton_cancelar.connect("clicked", self.BotonCancelar)

		self.boton_aceptar = self.builder.get_object("botonAceptar")
		self.boton_aceptar.connect("clicked", self.BotonAceptar)
		
		self.ventana0.show_all()
	def BotonCancelar(self, btn=None):
		self.ventana0.destroy()
	def BotonAceptar(self, btn=None):
		self.ventana0.destroy()	
	def BotonDirectorio(self, btn=None):
		d = Ventanadirectorio()
		# ~ f = Menu()
	def VerMoleculas(self, btn=None):
		print(" por ahora nada")	


class Ventana1():
	def __init__(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("moleculas.glade")

		self.ventana1= self.builder.get_object("ventana1")
		self.ventana1.connect("destroy", Gtk.main_quit)
		self.ventana1.set_default_size(600, 400)
		self.ventana1.set_title("Ventana Principal")

		self.boton_cargar= self.builder.get_object("cargarMolecula")
		self.boton_cargar.connect("clicked", self.CargarMolecula)
		

		self.boton_lista = self.builder.get_object("lista")
		self.boton_lista.connect("clicked", self.ListaMoleculas)
		self.ventana1.show_all()
	
	
	def importFile(self, *args):
		
		fn = gtk.FileChooserDialog(title="Import File",
								action=gtk.FILE_CHOOSER_ACTION_OPEN,
								buttons=
	(gtk.STOCK_CANCEL,gtk.RESPONSE_CANCEL,gtk.STOCK_OPEN,gtk.RESPONSE_OK))
		_filter = gtk.FileFilter()
		_filter.set_name("mol Files")
		_filter.add_pattern("*.mol2" )
		_filter.add_pattern("*.mol2")
		_filter.add_pattern("*.pdb")
		
		fn.add_filter(_filter)
		_filter = gtk.FileFilter()
		_filter.set_name("All Files")
		_filter.add_pattern("*")
		fn.add_filter(_filter)
		fn.show()

		resp = fn.run()
		
		
		if resp == gtk.RESPONSE_OK:
			text = open(fn.get_filename()).read()

			self.addNotebookPage(os.path.basename(fn.get_filename()), text)
		fn.destroy()
		
	
	def CargarMolecula (self, btn=None):
		
		p = Menu()
		
		
	def ListaMoleculas(self, btn=None):
		d = Lista()
		
class Molecula():
	def __init__(self,directorio):
		
		self.nombre =  directorio
		self.imagenName = self.crearimagen()
		
	def crearimagen(self): 
		listDir = self.nombre.split('/')
		pdb_name =listDir[len(listDir)-1]
		print(pdb_name)
		pymol.finish_launching()
		pymol.cmd.load(self.nombre)
		#pymol.cmd.disable("all")
		#pymol.cmd.enable(pdb_name)
		#pymol.cmd.viewport(1024,768)
		#pymol.cmd.hide('all')
		#pymol.cmd.show()
		#pymol.cmd.set('ray_opaque_background', 0)
		#pymol.cmd.color('red', 'ss h')
		#pymol.cmd.color('yellow', 'ss s')
		pymol.cmd.mpng(pdb_name+'.png', 1,1)
		# ~ pymol.cmd.close_all()

		#pymol.cmd.mplay()
		print('play')
		
		# ~ pymol.cmd.mpng("Ejemplo.png", width = 10 , height = 8, dpi = 300, ray=0)
		# ~ pymol.cmd.quit()
		#cmd.load(self.nombre)
		#cmd.show('cartoon')
		#cmd.png('mostrar.png')
		#cmd.quit()
		print(self.nombre)
		return 'nombre.jpg'
	
		
class Ventanadirectorio():
	def __init__(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("moleculas.glade")

		self.ventana2= self.builder.get_object("ventanadirectorio")
		self.ventana2.connect("destroy", Gtk.main_quit)
		self.ventana2.set_default_size(600, 400)
		self.ventana2.set_title("Ventana Principal")

		self.boton_cancelar = self.builder.get_object("botoncancelar")
		self.boton_cancelar.connect("clicked", self.cancelar)

		self.boton_aceptar = self.builder.get_object("botonaceptar")
		self.boton_aceptar.connect("clicked", self.aceptar)
		
		_filter = Gtk.FileFilter()
		_filter.set_name("mol Files")
		_filter.add_pattern("*.pdb")
		_filter.add_pattern( "*.mol2")
		_filter.add_pattern("*.mol")
		self.ventana2.add_filter(_filter)
		_filter = Gtk.FileFilter()
		_filter.set_name("All Files")
		_filter.add_pattern("*")
		self.ventana2.add_filter(_filter)

		
		self.ventana2.show_all()

	def aceptar(self, btn=None):
		print("self.nombre el boton add")
		#d = Lista()
		#response = d.ventana2.run()
		directorio = self.ventana2.get_filename()
		
		newMolucula = Molecula(directorio)

		#if response == Gtk.ResponseType.OK:
		#	print("self.nombre OK")
            
		#elif response == Gtk.ResponseType.CANCEL:
		#	print("self.nombre Cancelar")

	
	def cancelar(self, btn=None):
		self.ventana2.destroy()
		
class  Lista():
	def __init__(self):
		
		self.builder = Gtk.Builder()
		self.builder.add_from_file("moleculas.glade")
		self.Ventanalista = self.builder.get_object("Lista1")
		self.Ventanalista.set_title("Lista de Moleculas")
			   
		self.listmodel = Gtk.ListStore(str, str, str,str)
		self.treeResultado = self.builder.get_object("ventanadirectorio")
		self.boton_ver = self.builder.get_object("botonvisualizar")
		self.boton_ver.connect("clicked", self.visualizar)
		cell = Gtk.CellRendererText()
		title = ("Nombre", "Estructura", "% elementos", "ruta")
		#		for i in range(len(title)):
			
#			col = Gtk.TreeViewColumn(title[i], cell, text=i)
#			self.treeResultado.append_column(col)

#			self.show_all_data()
#			self.Lista.show_all()
	def visualizar(self, btn=None):
		print ("visualizar")

        
	def show_all_data(self):
		data = archivojson()
		for i in data:
			x = [x for x in i.values()]
			self.listmodel.append(x)

   		
if __name__ == "__main__":
	
	d = Ventana1()
	Gtk.main()
